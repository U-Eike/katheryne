import discord
import os
import sys
import json
import uuid
import tabulate
import cogs.Kath_Tools as KT
import math
from discord.ext import commands


mydir = os.path.dirname(__file__) or '.' 
inpath = os.path.join(mydir, 'Item_EN.json')
searchpath = os.path.join(mydir, 'Search_EN.json')
with open(inpath) as f:
    fdata = json.load(f)
    idata = fdata["Mats"]
    fdata = fdata["Food"]

Furn_Data = KT.loader('Furn_EN.json')["Pieces"]
Set_Data = KT.loader('Furn_EN.json')["Sets"]

Material_Search_Dict = KT.SearchComp("Mats")
Food_Search_Dict = KT.SearchComp("Food")
Furn_Search_Dict = KT.SearchComp("Furniture")
Set_Search_Dict = KT.SearchComp("Sets")

FSD = Food_Search_Dict[0] #Search Dict - With Int
FLSD = Food_Search_Dict[1] #Lower Search Dict
FISD = Food_Search_Dict[2] #Int Search Dict
FCon = Food_Search_Dict[3]

MSD = Material_Search_Dict[0] #Search Dict
MLSD = Material_Search_Dict[1] #Lower Search Dict
MISD = Material_Search_Dict[2] #Int Search Dict
MCon = Material_Search_Dict[3]

FuSD = Furn_Search_Dict[0] #Search Dict
FuLSD = Furn_Search_Dict[1] #Lower Search Dict
FuISD = Furn_Search_Dict[2] #Int Search Dict
FuCon = Furn_Search_Dict[3]

SuSD = Set_Search_Dict[0] #Search Dict
SuLSD = Set_Search_Dict[1] #Lower Search Dict
SuISD = Set_Search_Dict[2] #Int Search Dict
SuCon = Set_Search_Dict[3]

#Null Field Fix
def nf(item):
    if(item == ""):
        item = "** **"
    return(item)


def food(code,id="1"):
    if fdata[code]["Unique"]:
        embed = discord.Embed(
            title=fdata[code]["Variants"][int(id)]["Name"]
        )
        Art = "https://gitlab.com/U-Eike/katheryne/-/raw/master/Images/Food/"+fdata[code]["Variants"][int(id)]["Name"].replace(" ","_").replace("\"","").replace("'","")+"/Raw.png"
        embed.set_thumbnail(url=Art)
        embed.add_field(name="Description",      value=nf(fdata[code]["Variants"][int(id)]["Description"]),     inline=False)
        embed.add_field(name="Effect",      value=nf(fdata[code]["Variants"][int(id)]["Effect"]),     inline=False)
        Head = ["Material","# Req",]
        Final = tabulate.tabulate(fdata[code]["Recipe"], headers=Head)
        embed.add_field(name="**Cooking Ingredients**",      value="```"+Final+"```",     inline=False)
        embed.set_footer(text="Food | "+code)
    else:
        embed = discord.Embed(
            title=code
        )
        embed.add_field(name="Description",      value=nf(fdata[code]["Description"]),     inline=False)
        embed.add_field(name="Effect",      value=nf(fdata[code]["Effect"]),     inline=False)
        embed.set_footer(text="Food | "+code)
        Art = "https://gitlab.com/U-Eike/katheryne/-/raw/master/Images/Food/"+fdata[code]["Name"].replace(" ","_").replace("\"","").replace("'","")+"/Raw.png"
        embed.set_thumbnail(url=Art)

    return(embed)

def item(code):
    embed = discord.Embed(
        title=code
        #description="**__"+[id]+"__**"
    )
    Art = "https://gitlab.com/U-Eike/katheryne/-/raw/master/Images/Mats/"+code.replace(" ","_")+"/Raw.png"
    print(Art)
    embed.set_thumbnail(url=Art)
    embed.add_field(name="Description",      value=nf(idata[code]["Description"]),     inline=False)
    if idata[code]["Group"] != True:
        pass
        #embed.add_field(name="Tree",      value=nf(idata[code]["Group"]),     inline=False)
    else:
        pass
    if len(idata[code]["Weapons"]) != 0:
        Head = ["Weapon","# Req",]
        Final = tabulate.tabulate(idata[code]["Weapons"], headers=Head)
        embed.add_field(name="**Used For These Weapons**",      value="```"+Final+"```",     inline=False)

    if len(idata[code]["Characters"]) != 0:
        Head = ["Character","# Req",]
        Final = tabulate.tabulate(idata[code]["Characters"], headers=Head)
        embed.add_field(name="**Used For These Characters Ascensions**",      value="```"+Final+"```",     inline=False)

    if len(idata[code]["Food Recipes"]) != 0:
        Head = ["Recipe","# Req",]
        Final = tabulate.tabulate(idata[code]["Food Recipes"], headers=Head)
        embed.add_field(name="**Used For These Recipes**",      value="```"+Final+"```",     inline=False)

    if len(idata[code]["CharTalents"]) != 0:
        Head = ["Character","# Req",]
        Final = tabulate.tabulate(idata[code]["CharTalents"], headers=Head)
        embed.add_field(name="**Used For These Characters Skills (x3)**",      value="```"+Final+"```",     inline=False)
    
    if len(idata[code]["Furnitures"]) != 0:
        Head = ["Furnishing","# Req",]
        Final = tabulate.tabulate(idata[code]["Furnitures"], headers=Head)
        print(len(Final))
        if len(Final) < 1024:
            #print(" This Is The Bad Place")
            embed.add_field(name="**Used In These Furniture Recipes**",      value="```"+Final+"```",     inline=False)
        else:
            SPC = math.ceil(len(Final)/1024)
            SP = math.ceil(len(idata[code]["Furnitures"])/SPC)
            Head = ["Furnishing","# Req",]
            I = 0
            #print(str(SP)+" This Is The SP Value")
            while I <= SPC-1:
                #print("Here")
                if I == 0:
                    Final = tabulate.tabulate(idata[code]["Furnitures"][SP*I:SP*(I+1)], headers=Head)
                    embed.add_field(name="**Used In These Furniture Recipes**",      value="```"+Final+"```",     inline=False)
                else:
                    Final = tabulate.tabulate(idata[code]["Furnitures"][SP*I:SP*(I+1)], headers=Head)
                    embed.add_field(name="‎",      value="```"+Final+"```",     inline=False)
                #print(len(Final))
                I+=1
    if len(idata[code]["Sources"]) != 0:
        embed.add_field(name="**Sources**",      value="```"+"\n".join(idata[code]["Sources"])+"```",     inline=False)

    #Art = "https://gitlab.com/U-Eike/katheryne/-/raw/master/Images/Furniture/"+KT.FNC(Furn_Data[code]["Name"])+"_Card.png"
    #embed.set_image(url=Art)
    #print(Art)

    embed.set_footer(text="Item | "+code)
    return(embed)


def furn(code):
    embed = discord.Embed(
        title=code
        #description="**__"+[id]+"__**"
    )
    embed.add_field(name="Description",      value=nf(Furn_Data[code]["Desc"]),     inline=False)
    embed.add_field(name="Rarity",      value=nf(Furn_Data[code]["Rarity"]),     inline=False)
    embed.add_field(name="Comfort",      value=nf(Furn_Data[code]["Comfort"]),     inline=False)
    #embed.add_field(name="Cost",      value=nf(Furn_Data[code]["Cost"]),     inline=False)
    if "Crafting" in Furn_Data[code]:
        Table = [["Material","# Req"]]
        for item in Furn_Data[code]["Crafting"]["Recipe"]:
            Table.append([item,Furn_Data[code]["Crafting"]["Recipe"][item]])
        Final = tabulate.tabulate(Table, headers="firstrow")
        embed.add_field(name="**Recipe**",      value="```"+Final+"```",     inline=False)
        embed.add_field(name="**Build Time**",      value=Furn_Data[code]["Crafting"]["Crafting Time"],     inline=False)
    #if idata[code]["Group"] != True:
        #pass
        #embed.add_field(name="Tree",      value=nf(idata[code]["Group"]),     inline=False)
    #else:
        #pass
    if len(Furn_Data[code]["Sets"]) != 0:
        Table = [["Furniture Set","# Req"]]
        for item in Furn_Data[code]["Sets"]:
            Table.append([item,Furn_Data[code]["Sets"][item]])
        Final = tabulate.tabulate(Table, headers="firstrow")
        embed.add_field(name="**Included In These Furniture Sets**",      value="```"+Final+"```",     inline=False)
    Art = "https://gitlab.com/U-Eike/katheryne/-/raw/master/Images/Furniture/"+KT.FNC(Furn_Data[code]["Name"])+"_Card.png"
    embed.set_image(url=Art)
    print(Art)

    embed.set_footer(text="Furniture | "+code)
    return(embed)

def furnsets(code):
    embed = discord.Embed(
        title=code
        #description="**__"+[id]+"__**"
    )
    Art = "https://gitlab.com/U-Eike/katheryne/-/raw/master/Images/Items/Mats/"+code.replace(" ","_")+"/Raw.png"
    print(Art)
    embed.set_thumbnail(url=Art)
    embed.add_field(name="Description",      value=nf(Set_Data[code]["Desc"]),     inline=False)
    #embed.add_field(name="Cost",      value=nf(Furn_Data[code]["Cost"]),     inline=False)
    #if idata[code]["Group"] != True:
        #pass
        #embed.add_field(name="Tree",      value=nf(idata[code]["Group"]),     inline=False)
    #else:
        #pass
    Table = [["Furniture Piece","# Req"]]
    for item in Set_Data[code]["Pieces"]:
        Table.append([item,Set_Data[code]["Pieces"][item]])
    Final = tabulate.tabulate(Table, headers="firstrow")
    embed.add_field(name="**Pieces**",      value="```"+Final+"```",     inline=False)

    embed.set_footer(text="Furniture | "+code)
    return(embed)

def freturns(matches):


    embed = discord.Embed(
        title="Search Return Matches",
        description="For What You Searched For This Is What Was Closest\nPlease use k.(command) {id} of the item from the below list to get the result you were looking for"
    )

    print(matches)
    for ret in matches:
        det = ""
        det += "**Quick ID** : "+str(FCon[ret])+"\n"
        embed.add_field(name=ret,      value=det,     inline=False)

    return(embed)

def ireturns(matches):


    embed = discord.Embed(
        title="Search Return Matches",
        description="For What You Searched For This Is What Was Closest\nPlease use k.(command) {id} of the item from the below list to get the result you were looking for"
    )

    print(matches)
    for ret in matches:
        det = ""
        det += "**Quick ID** : "+str(MCon[ret])+"\n"
        embed.add_field(name=ret,      value=det,     inline=False)

    return(embed)

def Furn_Returns(matches):


    embed = discord.Embed(
        title="Search Return Matches",
        description="For What You Searched For This Is What Was Closest\nPlease use k.(command) {id} of the item from the below list to get the result you were looking for"
    )

    print(matches)
    for ret in matches:
        det = ""
        det += "**Quick ID** : "+str(FuCon[ret])+"\n"
        embed.add_field(name=ret,      value=det,     inline=False)

    return(embed)

def Set_Returns(matches):


    embed = discord.Embed(
        title="Search Return Matches",
        description="For What You Searched For This Is What Was Closest\nPlease use k.(command) {id} of the item from the below list to get the result you were looking for"
    )

    print(matches)
    for ret in matches:
        det = ""
        det += "**Quick ID** : "+str(SuCon[ret])+"\n"
        embed.add_field(name=ret,      value=det,     inline=False)

    return(embed)


def fsearch(code):
    print("Data For Food "+code)
    if code in FSD:
        found = True
        return([[code], True])
    else:
        try:
            code = int(code)
        except:
            pass
        if (type(code) == int):
            if (int(code) in range(0,len(FISD))):
                code = FSD[code]
                found = True
                return([[code], True])

        else:
            found = False
            returns = []
            que = code.lower()
            for item in FLSD:
                if que in item:
                    returns.append(FSD[FLSD.index(item)])

            if len(returns) == 1:
                found = True
                code = returns[0]
                return([[code], True])
            else:
                return([returns, False])

def Furn_Search(code):
    print("Data For Furniture "+code)
    if code in FuSD:
        found = True
        return([[code], True])
    else:
        try:
            code = int(code)
        except:
            pass
        if (type(code) == int):
            if (int(code) in range(0,len(FuISD))):
                code = FuSD[code]
                found = True
                return([[code], True])

        else:
            found = False
            returns = []
            que = code.lower()
            for item in FuLSD:
                if que in item:
                    returns.append(FuSD[FuLSD.index(item)])

            if len(returns) == 1:
                found = True
                code = returns[0]
                return([[code], True])
            else:
                return([returns, False])

def Sets_Search(code):
    print("Data For Sets "+code)
    if code in SuSD:
        found = True
        return([[code], True])
    else:
        try:
            code = int(code)
        except:
            pass
        if (type(code) == int):
            if (int(code) in range(0,len(SuISD))):
                code = SuSD[code]
                found = True
                return([[code], True])

        else:
            found = False
            returns = []
            que = code.lower()
            for item in SuLSD:
                if que in item:
                    returns.append(SuSD[SuLSD.index(item)])

            if len(returns) == 1:
                found = True
                code = returns[0]
                return([[code], True])
            else:
                return([returns, False])

def isearch(code):
    print("Data For Mat "+code)
    if code in MSD:
        found = True
        return([[code], True])
    else:
        try:
            code = int(code)
        except:
            pass
        if (type(code) == int):
            if (int(code) in range(0,len(MISD))):
                code = MSD[code]
                found = True
                return([[code], True])

        else:
            found = False
            returns = []
            que = code.lower()
            for item in MLSD:
                if que in item:
                    returns.append(MSD[MLSD.index(item)])

            if len(returns) == 1:
                found = True
                code = returns[0]
                return([[code], True])
            else:
                return([returns, False])




class Item_Commands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot



    @commands.command(name='food',brief="Food Set Details")
    async def food(self , ctx,  *args):

        print("Data For Unit ")
        code = args[0]
        try:
            print(args[1])
        except:
            pass
        hits = fsearch(code)
        if(len(hits[0]) == 1):
            code = hits[0][0]
            mess = await ctx.send(embed=food(code))
            if fdata[code]["Unique"]:
                await mess.add_reaction("🔴")
                await mess.add_reaction("🟡")
                await mess.add_reaction("🟢")
                if fdata[code]["Specialty"][0]:
                    await mess.add_reaction("⭐")
            elif fdata[code]["Unique"]:
                await mess.add_reaction("🔴")
                await mess.add_reaction("🟡")
                await mess.add_reaction("🟢")
            else:
                pass

        else:
            embed = freturns(hits[0])
            await ctx.send(embed=embed)


    @commands.command(name='item',brief="Food Set Details")
    async def materials(self , ctx,  *args):

        print("Data For Item ")
        code = " ".join(args)
        try:
            print(args[1])
        except:
            pass
        hits = isearch(code)
        if(len(hits[0]) == 1):
            code = hits[0][0]
            mess = await ctx.send(embed=item(code))
            """
            if len(fdata[code]) == 1:
                pass
            if len(fdata[code]) == 3:
                await mess.add_reaction("🔴")
                await mess.add_reaction("🟡")
                await mess.add_reaction("🟢")
            if len(fdata[code]) == 4:
                await mess.add_reaction("🔴")
                await mess.add_reaction("🟡")
                await mess.add_reaction("🟢")
                await mess.add_reaction("⭐")
            """

        else:
            embed = ireturns(hits[0])
            await ctx.send(embed=embed)

    @commands.command(name='furn',brief="Furniture Item Details",hidden=False)
    async def furniture(self , ctx,  *args):

        print("Data For Furniture ")
        code = " ".join(args)
        try:
            print(args[1])
        except:
            pass
        hits = Furn_Search(code)
        if(len(hits[0]) == 1):
            code = hits[0][0]
            mess = await ctx.send(embed=furn(code))

        else:
            embed = Furn_Returns(hits[0])
            await ctx.send(embed=embed)

    @commands.command(name='fset',brief="Furniture Sets Details",hidden=False)
    async def sets(self , ctx,  *args):

        print("Data For Furniture Set ")
        code = " ".join(args)
        try:
            print(args[1])
        except:
            pass
        hits = Sets_Search(code)
        if(len(hits[0]) == 1):
            code = hits[0][0]
            mess = await ctx.send(embed=furnsets(code))

        else:
            embed = Set_Returns(hits[0])
            await ctx.send(embed=embed)

############################Thanks To W0lf############################
    @commands.Cog.listener()
    async def on_raw_reaction_add(self,payload):
        #message_id #int – The message ID that got or lost a reaction.
        #user_id #int – The user ID who added or removed the reaction.
        #channel_id #int – The channel ID where the reaction got added or removed.
        #guild_id #Optional[int] – The guild ID where the reaction got added or removed, if applicable.
        #emoji #PartialEmoji – The custom or unicode emoji being used.
        try:
            Chanid = self.bot.get_channel(payload.channel_id)
            Messid = await self.bot.get_channel(payload.channel_id).fetch_message(payload.message_id)
        except:
            print('Failed to handle {payload},{Messid}\nJump-Url:{URL}'.format(
                    payload=payload,
                    Messid=payload.message_id,
                    URL='https://discordapp.com/channels/{server}/{channel}/{message}'.format(
                        server=payload.guild,
                        channel=payload.channel_id,
                        message=payload.message_id
                        )
                    )
                )
            return 0

        if payload.user_id != self.bot.user.id and Messid.author == self.bot.user:
            emoji = payload.emoji.name
            if emoji == "🗑️":
                await Messid.delete()
            ftext= (Messid.embeds[0].footer.text).split(" | ")
            etitle = Messid.embeds[0].author.name
            if ftext[0] == "Food":
                if emoji == "🔴":
                    await Messid.edit(embed=food(ftext[1],"1"))
                elif emoji == "🟡":
                    await Messid.edit(embed=food(ftext[1],"0"))
                elif emoji == "🟢":
                    await Messid.edit(embed=food(ftext[1],"2"))
                elif emoji == "⭐":
                    await Messid.edit(embed=food(ftext[1],"3"))

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self,payload):
        #message_id #int – The message ID that got or lost a reaction.
        #user_id #int – The user ID who added or removed the reaction.
        #channel_id #int – The channel ID where the reaction got added or removed.
        #guild_id #Optional[int] – The guild ID where the reaction got added or removed, if applicable.
        #emoji #PartialEmoji – The custom or unicode emoji being used.
        try:
            Chanid = self.bot.get_channel(payload.channel_id)
            Messid = await self.bot.get_channel(payload.channel_id).fetch_message(payload.message_id)
        except:
            print('Failed to handle {payload},{Messid}\nJump-Url:{URL}'.format(
                    payload=payload,
                    Messid=payload.message_id,
                    URL='https://discordapp.com/channels/{server}/{channel}/{message}'.format(
                        server=payload.guild,
                        channel=payload.channel_id,
                        message=payload.message_id
                        )
                    )
                )
            return 0

        if payload.user_id != self.bot.user.id and Messid.author == self.bot.user:
            emoji = payload.emoji.name
            ftext= (Messid.embeds[0].footer.text).split(" | ")
            etitle = Messid.embeds[0].author.name
            if ftext[0] == "Food":
                if emoji == "🔴":
                    await Messid.edit(embed=food(ftext[1],"1"))
                elif emoji == "🟡":
                    await Messid.edit(embed=food(ftext[1],"0"))
                elif emoji == "🟢":
                    await Messid.edit(embed=food(ftext[1],"2"))
                elif emoji == "⭐":
                    await Messid.edit(embed=food(ftext[1],"3"))

        

def setup(bot):
    bot.add_cog(Item_Commands(bot))



