import discord
import os
import sys
import json
import uuid
from discord.ext import commands


mydir = os.path.dirname(__file__) or '.' 
inpath = os.path.join(mydir, 'Item_EN.json')
FurniturePath = os.path.join(mydir, 'Furn_EN.json')
searchpath = os.path.join(mydir, 'Search_EN.json')


def loader(Loc):
    Loc = os.path.join(mydir , Loc)
    with open(Loc, encoding='utf-8') as temp:
        NTemp = json.load(temp)
    return(NTemp)

def FNC(FN):
    return FN.replace(" ","_").replace("\"","_").replace(":","_").replace("\\","_").replace("\t","").replace(":","")

SearchDicts = {}
BaseDict = loader('Search_EN.json')

for Tree in BaseDict:
    SearchDicts.update({Tree : [[],[],[],{}]})
    C = 0
    for item in BaseDict[Tree]:
        SearchDicts[Tree][0].append(item)
        SearchDicts[Tree][1].append(item.lower())
        SearchDicts[Tree][2].append(C)
        SearchDicts[Tree][3].update({item : C})
        C+=1
def SearchComp(Tree):
    return SearchDicts[Tree]

with open(inpath) as f:
    fdata = json.load(f)
    idata = fdata["Mats"]
    fdata = fdata["Food"]

with open(FurniturePath) as f:
    Furn_Data = json.load(f)