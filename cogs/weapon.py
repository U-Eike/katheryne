import discord
import os
import sys
import json
import uuid
import cogs.Kath_Tools as KT
import tabulate
from discord.ext import commands

mydir = os.path.dirname(__file__) or '.' 
inpath = os.path.join(mydir, 'Weap_EN.json')
searchpath = os.path.join(mydir, 'Search_EN.json')
EMJDict = KT.loader('EMJDict.json')

with open(inpath) as f:
    data = json.load(f)
with open(searchpath) as f:
    poi = json.load(f)
    SeaDic = poi["Weapon"]
    ISeaDic = list(poi["Weapon"].keys())
    LISeaDic = []
    for item in ISeaDic:
        LISeaDic.append(item.lower())
#Null Field Fix
with open(os.path.join(mydir, "TagDes.json")) as f:
    tags = json.load(f)
    tags = tags["Weapons"]


def nf(item):
    if(item == ""):
        item = "**N/A**"
    return(item)


def base(code):
    embed = discord.Embed(
        title=code#,
#        description="**__"+nf(data[code][id][0])+"__**",
#        url="https://www.gensh.in/characters/"+code.lower()

    )
    Icon = "https://gitlab.com/U-Eike/katheryne/-/raw/master/Images/Weapons/"+code.replace(" ","_")+"/Base.png"
    embed.set_thumbnail(url=Icon)

    embed.add_field(name="Rarity :",      value=nf(int(data[code]["Rarity"])*":star:"),     inline=False)
    embed.add_field(name="Substat",      value=nf(data[code]["Substat Type"]),     inline=False)
    embed.add_field(name="Description",      value=nf(data[code]["Description"]),     inline=False)
    embed.add_field(name="Passive Name",      value=nf(data[code]["Passive"]["Name"]),     inline=False)
    embed.add_field(name="Passive Data",      value=nf(data[code]["Passive"]["Refinement"]),     inline=False)


    Art = "https://gitlab.com/U-Eike/katheryne/-/raw/master/Images/Weapons/"+code.replace(" ","_")+"/Combo.png"
    
    embed.set_image(url=Art)
    embed.set_footer(text="Weapon Card | "+code)
    return(embed)

def ascension(code,ind="0"):

    Mats = []
    if ind == "0":
        embed = discord.Embed(
            title="Ascension Materials",
            description="Materials Needed For All Ascensions"
        )
    else:
        embed = discord.Embed(
            title="Ascension "+ind+" Materials",
            description="Materials Needed For Ranking Up"
        )
    det = [["Material","Amount"]]
    if ind == "0":
        for val in data[code]["Total"]:
            det.append([val,data[code]["Total"][val]])
    else:
        for val in data[code]["Ascension"][int(ind)-1]:
            det.append([val,data[code]["Ascension"][int(ind)-1][val]])

    
    
    
    Final = tabulate.tabulate(det, headers="firstrow",numalign="left",stralign="left")

    embed.add_field(name="Total",      value="```"+Final+"```",     inline=False)
    embed.set_footer(text="Weapon Ascension | "+code)
    return(embed)

def story(code):
    EL = len(data[code]["Story"])
    embed = discord.Embed(
        title=code

    )
    if EL > 1024 and EL != 0:
        pieces = data[code]["Story"].split("\n")
        for part in pieces:
            if part not in [""," ",]:
                embed.add_field(name="** **",      value=nf(part),     inline=False)
#        embed.add_field(name="Cont",      value=nf("\n".join(pieces[int(len(pieces)/2):])),     inline=False)
    else:
        embed.add_field(name="** **",      value=nf(data[code]["Story"]),     inline=False)
    embed.set_footer(text="Story | "+code)
    return(embed)



def stats(code):
    embed = discord.Embed(
        title=code#,
#        description="**__"+nf(data[code][id][0])+"__**",
#        url="https://www.gensh.in/characters/"+code.lower()

    )
    table = []
    C = 0
    for item in data[code]["Stats"]:
        table.append([item[0],item[1],item[2]])
        C+=1
    Head = ["Lvl","Base Atk",data[code]["Substat Type"]]
    Final = tabulate.tabulate(table, headers=Head)
    embed.add_field(name="**Stat Table**",      value="```"+Final+"```",     inline=False)
    embed.set_footer(text="Weapon Stats | "+code)

    return(embed)

def returns(matches):

    embed = discord.Embed(
        title="Search Return Matches",
        description="For What You Searched For This Is What Was Closest\nPlease use k.(command) {id} of the item from the below list to get the result you were looking for"
    )

    print(matches)
    for ret in matches:
            det = "Class - "+data[ret]["Class"]
            det += "\nRarity - "+(int(data[ret]["Rarity"])*":star:")
            det += "\nQuick ID - "+str(SeaDic[ret]["NID"])
            embed.add_field(name=ret,      value=det,     inline=False)

    return(embed)

def search(code):
    print("Data For Weapon "+code)
    if code in SeaDic.keys():
        found = True
        return([[code], True])
    else:
        try:
            code = int(code)
        except:
            pass
        if (type(code) == int):
            if (int(code) in range(0,len(ISeaDic))):
                code = ISeaDic[code]
                found = True
                return([[code], True])

        else:
            found = False
            returns = []
            que = code.lower()
            for item in LISeaDic:
                if que in item:
                    returns.append(ISeaDic[LISeaDic.index(item)])

            if len(returns) == 1:
                found = True
                code = returns[0]
                return([[code], True])
            else:
                return([returns, False])

def searchtags(code):
    print("Searching For Tag(s) "+str(code))
    print(code)
    returns = []
    for item in SeaDic:
        count = 0
        for tag in SeaDic[item]["Tags"]:
            for sea in code:
                if sea.lower() in tag.lower():
                    count+=1
                else:
                    pass
        if count >= len(code):
            returns.append(item)
        else:
            pass

    return([returns, False])


def rettags():
    emb = []
    embed = discord.Embed(
        title="Weapon Tags",
        description="These are the recorded tags for Weapons"
    )
    for item in tags:
        fie = ""
        for tagtype in tags[item]:
            fie += "**"+tagtype+"** : "+tags[item][tagtype]+"\n"
        embed.add_field(name="__"+item+"__",      value=fie,     inline=False)
    return(embed)



class Weapon_Commands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot



    @commands.command(name='weap'
    ,brief="Weapon Info"
    ,description="This command pulls up a weapons Info and stats"
    ,help="k.weap \"Weapon name\" eg k.weap Match\nk.weap \"Quick ID\" eg k.weap 16\nThe id can be found when you try to enter a weapon name and it matches multiple weapons")
    async def weapon(self , ctx, *args):

        if len(args) == 0:
            await ctx.send_help(Weapon_Commands.weapon)

        code = " ".join(args)
        hits = search(code)

        if(len(hits[0]) == 1):
            code = hits[0][0]
            embed = []
            embed.append(base(code))
            #embed.append(stats(code))
            for item in embed:
                mess = await ctx.send(embed=item)
            for Em in EMJDict["Weapon"]["All"]:
                await mess.add_reaction(Em)
        else:
            embed = returns(hits[0])
            await ctx.send(embed=embed)

    @commands.command(name='weapstory'
    ,brief="Weapon Story"
    ,description="This command pulls up a weapons Story"
    ,help="k.weap \"Weapon name\" eg k.weap Match\nk.weap \"Quick ID\" eg k.weap 16\nThe id can be found when you try to enter a weapon name and it matches multiple weapons")
    async def weaponstory(self , ctx, *args):

        if len(args) == 0:
            await ctx.send_help(Weapon_Commands.weapon)

        code = " ".join(args)
        hits = search(code)

        if(len(hits[0]) == 1):
            code = hits[0][0]
            embed = []
            embed.append(story(code))
            for item in embed:
                mess = await ctx.send(embed=item)
            for Em in EMJDict["Weapon"]["All"]:
                await mess.add_reaction(Em)
        else:
            embed = returns(hits[0])
            await ctx.send(embed=embed)

    @commands.command(name='weapstats'
    ,brief="Weapon Stat Table"
    ,description="This command pulls up a weapons Stat Table"
    ,help="k.weapstats \"Weapon name\" eg k.weapstats Match\nk.weapstats \"Quick ID\" eg k.weapstats 16\nThe id can be found when you try to enter a weapon name and it matches multiple weapons")
    async def weaponstats(self , ctx, *args):

        if len(args) == 0:
            await ctx.send_help(Weapon_Commands.weaponstats)


        code = " ".join(args)
        hits = search(code)
        if(len(hits[0]) == 1):
            code = hits[0][0]
            embed = []
            embed.append(stats(code))
            for item in embed:
                mess = await ctx.send(embed=item)
            for Em in EMJDict["Weap"]["All"]:
                await mess.add_reaction(Em)
        else:
            embed = returns(hits[0])
            await ctx.send(embed=embed)

    @commands.command(name='weapsea'
    ,brief="Search Weapons by tags"
    ,description="This command Searches for all weapons matching the input tags"
    ,help="k.weapsea \"Tags Seperated By space\" eg k.weapsea hp pole \n\nThis will give you all weapons that are a polearm and have hp as a substat.\nIf there is only one weapon matching the tags it will give you that instead\nA list of tags can be obtained using the command k.weaptags")
    async def weaponsearch(self , ctx, *args):

        if len(args) == 0:
            await ctx.send_help(Weapon_Commands.weaponsearch)
        else:

            code = list(args)
            hits = searchtags(code)
            if(len(hits[0]) == 1):
                code = hits[0][0]
                embed = []
                embed.append(base(code))
                for item in embed:
                    await ctx.send(embed=item)
            else:
                embed = returns(hits[0])
                await ctx.send(embed=embed)

    @commands.command(name='asc'
    ,hidden=False)
    async def ascension(self, ctx,  *args):


        if len(args) == 0:
            await ctx.send_help(Weapon_Commands.charactercard)
        else:

            code = args[0]
            #lev = args[1]
            hits = search(code)
            if(len(hits[0]) == 1):
                code = hits[0][0]
                embed = ascension(code)
                mess = await ctx.send(embed=embed)
                await mess.add_reaction("📊")
                await mess.add_reaction("1️⃣")
                await mess.add_reaction("1️⃣")
                await mess.add_reaction("2️⃣")
                await mess.add_reaction("3️⃣")
                await mess.add_reaction("4️⃣")
                await mess.add_reaction("5️⃣")
                await mess.add_reaction("6️⃣")
                #await mess.edit(embed=normalstats(code,str(10)))
            elif(len(hits[0]) == 1) and (len(args) == 2):
                code = hits[0][0]
                lev = args[1]
                embed = ascension(code,lev)
                mess = await ctx.send(embed=embed)
                await mess.add_reaction("📊")
                #await mess.edit(embed=normalstats(code,str(10)))


    @commands.command(name='weaptags',brief="List of weapon tags")
    async def weapontags(self , ctx, *args):

        item = rettags()
        await ctx.send(embed=item)

############################Thanks To W0lf############################
    @commands.Cog.listener()
    async def on_raw_reaction_add(self,payload):
        #message_id #int – The message ID that got or lost a reaction.
        #user_id #int – The user ID who added or removed the reaction.
        #channel_id #int – The channel ID where the reaction got added or removed.
        #guild_id #Optional[int] – The guild ID where the reaction got added or removed, if applicable.
        #emoji #PartialEmoji – The custom or unicode emoji being used.
        try:
            Chanid = self.bot.get_channel(payload.channel_id)
            Messid = await self.bot.get_channel(payload.channel_id).fetch_message(payload.message_id)
        except:
            print('Failed to handle {payload},{Messid}\nJump-Url:{URL}'.format(
                    payload=payload,
                    Messid=payload.message_id,
                    URL='https://discordapp.com/channels/{server}/{channel}/{message}'.format(
                        server=payload.guild,
                        channel=payload.channel_id,
                        message=payload.message_id
                        )
                    )
                )
            return 0
        if payload.user_id != self.bot.user.id and Messid.author == self.bot.user:
            emoji = payload.emoji.name
            if emoji == "🗑️":
                await Messid.delete()
            ftext= (Messid.embeds[0].footer.text).split(" | ")
            etitle = Messid.embeds[0].author.name
            #Skill Table And Description Transition Checks
            if ftext[0] == "Weapon Card":
                if emoji == "📊":
                    await Messid.edit(embed=stats(ftext[1]))
            if ftext[0] == "Weapon Stats":
                if emoji == "📊":
                    await Messid.edit(embed=base(ftext[1]))


    @commands.Cog.listener()
    async def on_raw_reaction_remove(self,payload):
        #message_id #int – The message ID that got or lost a reaction.
        #user_id #int – The user ID who added or removed the reaction.
        #channel_id #int – The channel ID where the reaction got added or removed.
        #guild_id #Optional[int] – The guild ID where the reaction got added or removed, if applicable.
        #emoji #PartialEmoji – The custom or unicode emoji being used.
        try:
            Chanid = self.bot.get_channel(payload.channel_id)
            Messid = await self.bot.get_channel(payload.channel_id).fetch_message(payload.message_id)
        except:
            print('Failed to handle {payload},{Messid}\nJump-Url:{URL}'.format(
                    payload=payload,
                    Messid=payload.message_id,
                    URL='https://discordapp.com/channels/{server}/{channel}/{message}'.format(
                        server=payload.guild,
                        channel=payload.channel_id,
                        message=payload.message_id
                        )
                    )
                )
            return 0

        if payload.user_id != self.bot.user.id and Messid.author == self.bot.user:
            emoji = payload.emoji.name
            if emoji == "🗑️":
                await Messid.delete()
            ftext= (Messid.embeds[0].footer.text).split(" | ")
            etitle = Messid.embeds[0].author.name
            #Skill Table And Description Transition Checks
            if ftext[0] == "Weapon Card":
                if emoji == "📊":
                    await Messid.edit(embed=stats(ftext[1]))
            if ftext[0] == "Weapon Stats":
                if emoji == "📊":
                    await Messid.edit(embed=base(ftext[1]))

def setup(bot):
    bot.add_cog(Weapon_Commands(bot))



