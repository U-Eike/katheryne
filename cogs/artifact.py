import discord
import os
import sys
import json
import uuid
from discord.ext import commands


mydir = os.path.dirname(__file__) or '.' 
inpath = os.path.join(mydir, 'Artif_EN.json')
searchpath = os.path.join(mydir, 'Search_EN.json')
with open(inpath) as f:
    data = json.load(f)
with open(searchpath) as f:
    poi = json.load(f)
    SeaDic = poi["Artifact"]
    ISeaDic = list(poi["Artifact"].keys())
    LISeaDic = []
    for item in ISeaDic:
        LISeaDic.append(item.lower())

    PSeaDic = poi["Piece"]
    PISeaDic = list(poi["Piece"].keys())
    PLISeaDic = []
    for item in PISeaDic:
        PLISeaDic.append(item.lower())
#Null Field Fix
spe = { "Goblet" : "Goblet of Eonothem",
        "Flower" : "Flower of Life",
        "Headpiece" : "Circlet of Logos",
        "Feather" : "Plume of Death",
        "Timepiece" : "Sands of Eon"}




def nf(item):
    if(item in [""]):
        print(item+"this")
        item = "*___________*"
    return(item)

def intro(code):
    embed = discord.Embed(
        title="Artifact Set",
        description=code,
        #url=unit['link'],
        #color=ELEMENT_COLOR.get(unit['element'], DEFAULT_ELEMENT_COLOR),
    )
    Art = "https://gitlab.com/U-Eike/katheryne/-/raw/master/Images/Artifacts/"+data[code]["SID"]+".gif"
    #embed.set_thumbnail(url=Icon)

    embed.set_image(url=Art)
    #unit data
    for item in data[code]["Bonuses"]:
        embed.add_field(name=item[0]+" Piece Set Effect",      value=item[1],     inline=False)
#            embed.add_field(name="Description : ",      value=nf(data[code]["Secret-Story"]),     inline=False)
    return(embed)


def short(code,id):
    EL = len(data[code]["Set Pieces"][id][1])
    embed = discord.Embed(
        title=data[code]["Set Pieces"][id][0],
        description="**__"+spe[id]+"__**"
    )
    Art = "https://gitlab.com/U-Eike/katheryne/-/raw/master/Images/Artifacts/"+data[code]["Set Pieces"][id][3]+".png"
    embed.set_image(url=Art)
    if EL > 1024 and EL != 0:
        pieces = data[code]["Set Pieces"][id][1].split("\n")
        for part in pieces:
            embed.add_field(name="** **",      value=nf(part),     inline=False)

    else:
        embed.add_field(name="** **",      value=nf(data[code]["Set Pieces"][id][1]),     inline=False)
    return(embed)

def longer(code,id):
    EL = len(data[code]["Set Pieces"][id][2])
    embed = discord.Embed(
        title=data[code]["Set Pieces"][id][0],
        description="**__"+spe[id]+"__**"
    )
    Art = "https://gitlab.com/U-Eike/katheryne/-/raw/master/Images/Artifacts/"+data[code]["Set Pieces"][id][3]+".png"
    embed.set_image(url=Art)
    if EL > 1024 and EL != 0:
        pieces = data[code]["Set Pieces"][id][2].split("\n")
        for part in pieces:
            embed.add_field(name="** **",      value=nf(part),     inline=False)

    else:
        embed.add_field(name="** **",      value=nf(data[code]["Set Pieces"][id][2]),     inline=False)
    return(embed)

def piecelore(code):
    Set = PSeaDic[code]["Set"]
    Slot = PSeaDic[code]["Part"]
    print(Set)
    EL = len(data[Set]["Set Pieces"][Slot][2])
    embed = discord.Embed(
        title=code,
        description="**__Part Of The "+Set+" Lineage__**"
#        url="https://www.gensh.in/characters/"+code.lower()
    )
    Art = "https://gitlab.com/U-Eike/katheryne/-/raw/master/Images/Artifacts/"+data[Set]["Set Pieces"][Slot][3]+".png"
    #embed.set_thumbnail(url=Icon)
    embed.set_image(url=Art)
    if EL > 1024 and EL != 0:
        embed.add_field(name="**Short Intro**",      value=nf(data[Set]["Set Pieces"][Slot][1]),     inline=False)
        embed.add_field(name="**Full Story**",      value=nf(data[Set]["Set Pieces"][Slot][2]),     inline=False)

        pieces = data[code][id]["Story"].split("\n")
        for part in pieces:
            embed.add_field(name="** **",      value=nf(part),     inline=False)

    else:
        embed.add_field(name="**Short Intro**",      value=nf(data[Set]["Set Pieces"][Slot][1]),     inline=False)
        embed.add_field(name="**Full Story**",      value=nf(data[Set]["Set Pieces"][Slot][2]),     inline=False)
    return(embed)


def returns(matches):


    embed = discord.Embed(
        title="Search Return Matches",
        description="For What You Searched For This Is What Was Closest\nPlease use k.(command) {id} of the item from the below list to get the result you were looking for"
    )

    print(matches)
    for ret in matches:
        det = ""
        for bon in data[ret]["Bonuses"]:
            det += "**"+bon[0]+" Piece Set Effect** : "+bon[1]+"\n"
        det += "**Quick ID** : "+str(SeaDic[ret]["NID"])+"\n"
        embed.add_field(name=ret,      value=det,     inline=False)

    return(embed)

def Preturns(matches):


    embed = discord.Embed(
        title="Search Return Matches",
        description="For What You Searched For This Is What Was Closest\nPlease use k.(command) {id} of the item from the below list to get the result you were looking for"
    )

    print(matches)
    for ret in matches:
        det = ""
        det += "**Quick ID** : "+str(PSeaDic[ret]["NID"])+"\n"
        embed.add_field(name=ret,      value=det,     inline=False)

    return(embed)



def search(code):
    print("Data For Artifact "+code)
    if code in SeaDic.keys():
        found = True
        return([[code], True])
    else:
        try:
            code = int(code)
        except:
            pass
        if (type(code) == int):
            if (int(code) in range(0,len(ISeaDic))):
                code = ISeaDic[code]
                found = True
                return([[code], True])

        else:
            found = False
            returns = []
            que = code.lower()
            for item in LISeaDic:
                if que in item:
                    returns.append(ISeaDic[LISeaDic.index(item)])

            if len(returns) == 1:
                found = True
                code = returns[0]
                return([[code], True])
            else:
                return([returns, False])


def searchP(code):
    print("Data For Artifact "+code)
    if code in PSeaDic.keys():
        found = True
        return([[code], True])
    else:
        try:
            code = int(code)
        except:
            pass
        if (type(code) == int):
            if (int(code) in range(0,len(PISeaDic))):
                code = PISeaDic[code]
                found = True
                return([[code], True])

        else:
            found = False
            returns = []
            que = code.lower()
            for item in PLISeaDic:
                if que in item:
                    returns.append(PISeaDic[PLISeaDic.index(item)])

            if len(returns) == 1:
                found = True
                code = returns[0]
                return([[code], True])
            else:
                return([returns, False])


class Artifact_Commands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot



    @commands.command(name='set',brief="Artifact Set Details")
    async def artifact(self , ctx,  *args):

        print("Data For Unit ")
        code = args[0]
        try:
            print(args[1])
        except:
            pass
        hits = search(code)
        if(len(hits[0]) == 1):
            code = hits[0][0]
            embed = [intro(code)]
            for item in embed:
                await ctx.send(embed=item)
        else:
            embed = returns(hits[0])
            await ctx.send(embed=embed)

    @commands.command(name='artsh',brief="Artifact Piece Intro Text")
    async def shortstory(self , ctx,  *args):

        print("Data For Unit ")
        code = args[0]
        try:
            print(args[1])
        except:
            pass
        hits = search(code)
        if(len(hits[0]) == 1):
            code = hits[0][0]
            embed = []
            for item in data[code]["Set Pieces"].keys():
                embed.append(short(code,item))
            for item in embed:
                await ctx.send(embed=item)
        else:
            embed = returns(hits[0])
            await ctx.send(embed=embed)

    @commands.command(name='artstory',brief="All Artifact Story Text and Flavor Text")
    async def fullstory(self , ctx,  *args):

        print("Data For Unit ")
        code = args[0]
        try:
            print(args[1])
        except:
            pass
        hits = search(code)
        if(len(hits[0]) == 1):
            code = hits[0][0]
            embed = []
            for item in data[code]["Set Pieces"].keys():
                embed.append(piecelore(data[code]["Set Pieces"][item][0]))
            for item in embed:
                await ctx.send(embed=item)
        else:
            embed = returns(hits[0])
            await ctx.send(embed=embed)

    @commands.command(name='artlo',brief="Detailed Artifact Story Text")
    async def longstory(self , ctx,  *args):

        print("Data For Unit ")
        code = args[0]
        try:
            print(args[1])
        except:
            pass
        hits = search(code)
        if(len(hits[0]) == 1):
            code = hits[0][0]
            embed = []
            for item in data[code]["Set Pieces"].keys():
                embed.append(longer(code,item))
            for item in embed:
                await ctx.send(embed=item)
        else:
            embed = returns(hits[0])
            await ctx.send(embed=embed)

    @commands.command(name='piece',brief="Story Of An Artifact Piece")
    async def piecestory(self , ctx,  *args):

        code = args[0]
        try:
            print(args[1])
        except:
            pass
        hits = searchP(code)
        if(len(hits[0]) == 1):
            code = hits[0][0]
            embed = piecelore(code)
            await ctx.send(embed=embed)
        else:
            embed = Preturns(hits[0])
            await ctx.send(embed=embed)

        

def setup(bot):
    bot.add_cog(Artifact_Commands(bot))



