import discord
import os
import sys
import json
import math
import uuid
import random

from discord import emoji
#import cogs.Kath_Tools as KT
import cogs.Kath_Tools as KT
import tabulate
from discord.ext import commands
from typing import List

mydir = os.path.dirname(__file__) or '.'

data = KT.loader('Char_EN.json')
EC = KT.loader('Elements.json')
EMJDict = KT.loader('EMJDict.json')
CAll = KT.loader('Search_EN.json')["Character"]
Char_Search_Dict = KT.SearchComp("Character")
CSD = Char_Search_Dict[0] #Search Dict - With Int
CLSD = Char_Search_Dict[1] #Lower Search Dict
CISD = Char_Search_Dict[2] #Int Search Dict
CCon = Char_Search_Dict[3]


with open(os.path.join(mydir, "TagDes.json")) as f:
    tags = json.load(f)
    tags = tags["Characters"]



EMC = {
    "Pyro":0xff9999,
    "Electro":0xffacff,
    "Hydro":0x80C0FF,
    "Anemo":0x80ffd7,
    "Geo":0xffe699,
    "Cryo":0x99ffff,
    "Dendro": 0xffe14b,
    "": 0xffe14b,
    "None": 0xffe14b
}

#Ban = []
# Null Field Fix
#Electro = <color=#FFE14BFF>
#Pyro = <color=#FF9999FF>
#Hydro = <color=#80C0FFFF>
#Anemo = <color=#80FFD7FF>
#Geo = <color=#FFE699FF>
#Cryo = <color=#99FFFFFF>
#Dendro = <color=#FFE14BFF>

def nf(item):
    if(item == ""):
        item = "** **"
    return(item)

#    embed.set_footer(text="Information requested by: {}".format(ctx.author.display_name))
def Char_Returns(matches,ind=0):

    EMBReturns = []
    if len(matches) < 25:
        embed = discord.Embed(
            title="Search Return Matches",
            description="For What You Searched For This Is What Was Closest\nPlease use k.(command) {id} of the item from the below list to get the result you were looking for"
        )

        print(matches)
        for ret in matches:
            det = "Element : "+data[ret]["Element"]
            det += "\nAffiliation : "+data[ret]["Affiliation"]
            det += "\nRarity : "+(int(data[ret]["Rarity"])*":star:")
            det += "\nWeapon : "+data[ret]["Class"]
            det += "\nQuick ID : "+str(CCon[ret])
            embed.add_field(name=ret+" - "+EC[data[ret]["Element"]][regi(data[ret]["Affiliation"])],      value=det,     inline=False)
        embed.set_footer(text="Search Returns | "+str(len(matches))+" | Matches Found")
        EMBReturns = embed
    else:
        embed = discord.Embed(
            title="Search Return Matches",
            description="For What You Searched For This Is What Was Closest\nPlease use k.(command) {id} of the item from the below list to get the result you were looking for"
        )

        print(matches)
        matches = [matches[:len(matches)//2],matches[len(matches)//2:]]
        for ret in matches:
            det = "Element : "+data[ret]["Element"]
            det += "\nAffiliation : "+data[ret]["Affiliation"]
            det += "\nRarity : "+(int(data[ret]["Rarity"])*":star:")
            det += "\nWeapon : "+data[ret]["Class"]
            det += "\nQuick ID : "+str(CCon[ret])
            embed.add_field(name=ret+" - "+EC[data[ret]["Element"]][regi(data[ret]["Affiliation"])],      value=det,     inline=False)
        embed.set_footer(text="Search Returns | "+str(len(matches))+" | Matches Found")
        EMBReturns = embed

    return(EMBReturns)

def Char_Search(code):
    print("Data For Character "+code)
    base = code
    if code in CCon.keys():
        found = True
        return([[code], True])
    else:
        try:
            code = int(code)
        except:
            pass
        if (type(code) == int):
            if (int(code) in range(0,len(CSD))):
                code = CSD[code]
                found = True
                return([[code], True])

        else:
            found = False
            returns = []
            que = code.lower()
            for item in CLSD:
                if que in item:
                    returns.append(CSD[CLSD.index(item)])

            if len(returns) == 1:
                found = True
                code = returns[0]
                return([[code], True])
            elif len(returns) == 0:
                return(searchtags([base]))
            else:
                return([returns, False])

def searchtags(code):
    print("Searching For Tag(s) "+str(code))
    print(code)
    returns = []
    for item in CAll:
        count = 0
        for tag in CAll[item]["Tags"]:
            for sea in code:
                if sea.lower() in tag.lower():
                    count+=1
                else:
                    pass
        if count >= len(code):
            returns.append(item)
        else:
            pass
    return([returns, False])


def stats(code):
    ICode = code.replace(" ","_")
    embed = discord.Embed(
        title=code,
        url="https://www.gensh.in/characters/"+ICode.lower(),
        color=EMC[data[code]["Element"]]

    )
    table = data[code]["Stats"]
    for item in table[0]:
        table[0][table[0].index(item)] = repla(item)

    Final = tabulate.tabulate(table, headers="firstrow")
    embed.add_field(name="**Stat Table**",      value="```"+Final+"```",     inline=False)
    embed.set_footer(text="Character Table | "+code)
    Art = "https://gitlab.com/U-Eike/katheryne/-/raw/master/Images/Characters/"+ICode+"/.png"
    embed.set_image(url=Art)

    return(embed)

def ranger(base,max):
    ret = []
    for item in range(base,max):
        ret.append(item)
    return ret

def ascension(code,ind="0"):
    ICode = code.replace(" ","_")

    Mats = []
    if ind == "0":
        embed = discord.Embed(
            title="Ascension Materials",
            description="Materials Needed For All Ascensions"
        )

        Art = "https://gitlab.com/U-Eike/katheryne/-/raw/master/Images/Characters/"+ICode+"/.png"
        embed.set_image(url=Art)

    else:
        embed = discord.Embed(
            title="Ascension "+ind+" Materials",
            description="Materials Needed For Ranking Up"
        )
    det = [["Material","Amount"]]
    if ind == "0":
        for val in data[code]["Total"]:
            det.append([val,data[code]["Total"][val]])
        Art = "https://gitlab.com/U-Eike/katheryne/-/raw/master/Images/Characters/"+ICode+"/ASC.png"
        embed.set_image(url=Art)

    else:
        for val in data[code]["Ascension"][int(ind)-1][1]:
            det.append([val["Id"],val["Count"]])

    
    
    
    Final = tabulate.tabulate(det, headers="firstrow",numalign="left",stralign="left")

    embed.add_field(name="Total",      value="```"+Final+"```",     inline=False)
    embed.set_footer(text="Character Ascension | "+code)
    
    return(embed)

def natk(code):
    ICode = code.replace(" ","_")

    embed = discord.Embed(
        title=code,
        description="**"+data[code]["Normal Atk"][0]+"**",
        url="https://www.gensh.in/characters/"+ICode.lower(),
        color=EMC[data[code]["Element"]]
    )
    Icon = "https://gitlab.com/U-Eike/katheryne/-/raw/master/Images/Characters/"+ICode+"/Skills/0.png"
    embed.set_thumbnail(url=Icon)
    pieces = data[code]["Normal Atk"][1].split("\n\n")
    for part in pieces:
        embed.add_field(name="** **",      value=nf(part),     inline=False)
    embed.set_footer(text="Skill Desc | "+code+" | Normal")
    return(embed)

def normalstats(code,id):
    ICode = code.replace(" ","_")
    embed = discord.Embed(
        title=code,
        url="https://www.gensh.in/characters/"+ICode.lower(),
        color=EMC[data[code]["Element"]]
    )
    source = data[code]["Normal Atk"][2]
    table = source[0:2]
    newt = [["Attribute","Modifiers"]]
    for item in ranger(0,len(source[0])):
        newt.append([source[0][item],source[int(id)][item]])
    Final = tabulate.tabulate(newt,headers="firstrow")

    embed.add_field(name="**Stat Table For Level "+id+" Normal Attack**",      value="```"+Final+"```",     inline=False)

    NID = int(id)
    if NID > 1 and NID < 11:
        source = data[code]["Elemental Burst"][4][NID-2][1]
        newt = [["Name","Amount"]]
        for item in source:
            newt.append([item,source[item]])
        Final = tabulate.tabulate(newt,headers="firstrow")
        embed.add_field(name="**Materials Required**",      value="```"+Final+"```",     inline=False)

    embed.set_footer(text="Skill Table | "+code+" | Normal | "+id)

    return(embed)

def skill(code):
    ICode = code.replace(" ","_")
    embed = discord.Embed(
        title=code,
        description="**Elemental Skill - "+data[code]["Elemental Skill"][0]+"**",
        url="https://www.gensh.in/characters/"+ICode.lower(),
        color=EMC[data[code]["Element"]]

    )
    Icon = "https://gitlab.com/U-Eike/katheryne/-/raw/master/Images/Characters/"+ICode+"/Skills/1.png"
    embed.set_thumbnail(url=Icon)

    pieces = data[code]["Elemental Skill"][1].split("\n\n")
    for part in pieces:
        embed.add_field(name="** **",      value=nf(part),     inline=False)
    embed.set_footer(text="Skill Desc | "+code+" | Skill")

    return(embed)

def skillstats(code,id):
    ICode = code.replace(" ","_")
    embed = discord.Embed(
        title=code,
        url="https://www.gensh.in/characters/"+ICode.lower(),
        color=EMC[data[code]["Element"]]
    )
    source = data[code]["Elemental Skill"][2]
    table = source[0:2]
    newt = [["Attribute","Modifiers"]]
    for item in ranger(0,len(source[0])):
        newt.append([source[0][item],source[int(id)][item]])
    Final = tabulate.tabulate(newt,headers="firstrow")

    embed.add_field(name="**Stat Table For Level "+id+" Elemental Skill**",      value="```"+Final+"```",     inline=False)


    NID = int(id)
    if NID > 1 and NID < 11:
        source = data[code]["Elemental Burst"][4][NID-2][1]
        newt = [["Name","Amount"]]
        for item in source:
            newt.append([item,source[item]])
        Final = tabulate.tabulate(newt,headers="firstrow")
        embed.add_field(name="**Materials Required**",      value="```"+Final+"```",     inline=False)
    embed.set_footer(text="Skill Table | "+code+" | Skill | "+id)

    return(embed)

def burst(code):
    ICode = code.replace(" ","_")
    embed = discord.Embed(
        title=code,
        description="**Elemental Burst - "+data[code]["Elemental Burst"][0]+"**",
        url="https://www.gensh.in/characters/"+ICode.lower(),
        color=EMC[data[code]["Element"]]

    )
    Icon = "https://gitlab.com/U-Eike/katheryne/-/raw/master/Images/Characters/"+ICode+"/Skills/2.png"
    embed.set_thumbnail(url=Icon)
    pieces = data[code]["Elemental Burst"][1].split("\n\n")
    for part in pieces:
        embed.add_field(name="** **",      value=nf(part),     inline=False)
    embed.set_footer(text="Skill Desc | "+code+" | Burst")

    return(embed)

def burststats(code,id):
    ICode = code.replace(" ","_")
    embed = discord.Embed(
        title=code,
        url="https://www.gensh.in/characters/"+ICode.lower(),
        color=EMC[data[code]["Element"]]
    )
    source = data[code]["Elemental Burst"][2]
    table = source[0:2]
    newt = [["Attribute","Modifiers"]]
    for item in ranger(0,len(source[0])):
        newt.append([source[0][item],source[int(id)][item]])
    Final = tabulate.tabulate(newt,headers="firstrow")
    embed.add_field(name="**Stat Table For Level "+id+" Elemental Burst**",      value="```"+Final+"```",     inline=False)

    NID = int(id)
    if NID > 1 and NID < 11:
        source = data[code]["Elemental Burst"][4][NID-2][1]
        newt = [["Name","Amount"]]
        for item in source:
            newt.append([item,source[item]])
        Final = tabulate.tabulate(newt,headers="firstrow")
        embed.add_field(name="**Materials Required**",      value="```"+Final+"```",     inline=False)

    embed.set_footer(text="Skill Table | "+code+" | Burst | "+id)

    return(embed)

def total(code,typ):
    ICode = code.replace(" ","_")
    if typ == "Char":
        embed = discord.Embed(
            title=code,
            description="**Character Ascension Materials**",
            url="https://www.gensh.in/characters/"+ICode.lower(),
            color=EMC[data[code]["Element"]]

        )
        source = data[code]["Elemental Burst"][3]
        newt = [["Name","Amount"]]
        for val in data[code]["Total"]:
            newt.append([val,data[code]["Total"][val]])
        Final = tabulate.tabulate(newt,headers="firstrow")
        embed.add_field(name="**Materials Required In Total**",      value="```"+Final+"```",     inline=False)

        Art = "https://gitlab.com/U-Eike/katheryne/-/raw/master/Images/Characters/"+ICode+"/ASC.png"
        embed.set_image(url=Art)
        embed.set_footer(text="Char B | "+code)
    elif typ == "Tal":
        embed = discord.Embed(
            title=code,
            description="**Character Talent Upgrade Materials**",
            url="https://www.gensh.in/characters/"+ICode.lower(),
            color=EMC[data[code]["Element"]]

        )
        source = data[code]["Elemental Burst"][3]
        newt = [["Name","Amount"]]
        for item in source:
            newt.append([item,source[item]])
        Final = tabulate.tabulate(newt,headers="firstrow")
        embed.add_field(name="**Materials Required In Total**",      value="```"+Final+"```",     inline=False)

        Art = "https://gitlab.com/U-Eike/katheryne/-/raw/master/Images/Characters/"+ICode+"/TAL.png"
        embed.set_image(url=Art)
        embed.set_footer(text="Tal B | "+code)
    return(embed)


def rettags():
    emb = []
    embed = discord.Embed(
        title="Character Tags",
        description="These are the recorded tags for Characters"
    )
    for item in tags:
        fie = ""
        for tagtype in tags[item]:
            fie += "**"+tagtype+"** : "+tags[item][tagtype]+"\n"
        embed.add_field(name="__"+item+"__",      value=fie,     inline=False)
    return(embed)



def regi(loc):
    if loc in ["Knights of Favonius","Church of Favonius","Adventurers' Guild","Dawn Winery","The Cat's Tail","Wolvendom"]:
        return "Mondstadt"
    elif loc in ["The Crux","Liyue Harbor","Yuehai Pavilion","Liyue Qixing","Mondstadt","Bubu Pharmacy","Fatui","Wanmin Restaurant","Feiyun Commerce Guild","Liyue Adeptus"]:
        return "Liyue"
    else:
        return "Other"


def intro(code):
    embed = discord.Embed(
        color=EMC[data[code]["Element"]]
    )
    tcode = EC[data[code]["Element"]][regi(data[code]["Affiliation"])].split(":")[2]
    tcode = tcode[:len(tcode)-1]
    tcode = "https://cdn.discordapp.com/emojis/"+tcode+".png"
    ICode = code.replace(" ","_")
    print(code)
    embed.set_author(name=code,
                     icon_url=tcode,
                     url="https://www.gensh.in/characters/"+ICode.lower())

    Icon = "https://gitlab.com/U-Eike/katheryne/-/raw/master/Images/Characters/Cards/"+ICode+".png"
    embed.set_thumbnail(url=Icon)
    embed.add_field(name="Title :",      value=nf(data[code]["Title"]),     inline=False)
    embed.add_field(name="Introduction",      value=nf(data[code]["Intro"]),     inline=False)
    embed.add_field(name="Constellation :",      value=nf(data[code]["Constellation"]),     inline=False)
    embed.add_field(name="Affiliation :",      value=nf(data[code]["Affiliation"]),     inline=False)
    embed.add_field(name="Element :",      value=nf(data[code]["Element"]),     inline=False)
    embed.add_field(name="Weapon :",      value=nf(data[code]["Class"]),     inline=False)
    embed.add_field(name="Rarity :",      value=nf(int(data[code]["Rarity"])*":star:"),     inline=False)
    newt = [["Name","Amount"]]
    for val in data[code]["Total"]:
        newt.append([val,data[code]["Total"][val]])
    Final = tabulate.tabulate(newt,headers="firstrow")
    embed.add_field(name="**Materials Required To Fully Ascend  **",      value="```"+Final+"```",     inline=False)

    Art = "https://gitlab.com/U-Eike/katheryne/-/raw/master/Images/Characters/"+ICode+"/CB.jpg"

    embed.set_image(url=Art)
    embed.set_footer(text="Intro | "+code)

    return(embed)

#def Usage(ctx):
#    if 


def history(code,id=0):
    ICode = code.replace(" ","_")
    tra = ["0","1","2","3","4","5","Fave","Focus"]
    traID = tra[int(id)]
    EL = len(data[code]["Story"][traID][1])
    embed = discord.Embed(
        title=code,
        description="**__"+nf(data[code]["Story"][traID][0])+"__**",
        url="https://www.gensh.in/characters/"+ICode.lower(),
        color=EMC[data[code]["Element"]]

    )
    if EL > 1024 and EL != 0:
        pieces = data[code]["Story"][traID][1].split("\n")
        for part in pieces:
            embed.add_field(name="** **",      value=nf(part),     inline=False)
#        embed.add_field(name="Cont",      value=nf("\n".join(pieces[int(len(pieces)/2):])),     inline=False)

    else:
        embed.add_field(name="** **",      value=nf(data[code]["Story"][traID][1]),     inline=False)

    Art = "https://gitlab.com/U-Eike/katheryne/-/raw/master/Images/Characters/"+ICode+"/Raw_Bar.png"
    embed.set_image(url=Art)

    embed.set_footer(text="Story | "+str(id)+" | "+code+" | 7")
    return(embed)

Pas = {
    1:"Ascension 2 Passive",
    2:"Ascension 4 Passive",
    3:"Open World Passive"
}

def stars(code,id="0"):
    if id == "0":
        ICode = code.replace(" ","_")
        embed = discord.Embed(
            title=code,
            description="Constellations for "+code,
            url="https://www.gensh.in/characters/"+ICode.lower(),
            color=EMC[data[code]["Element"]]
        )
        Icon = "https://gitlab.com/U-Eike/katheryne/-/raw/master/Images/Characters/"+ICode+"/Talents/"+id+".png"
        embed.set_thumbnail(url=Icon)
        C = 1
        for const in data[code]["Talents"]:
            embed.add_field(name=nf(data[code]["Talents"][const][0]),      value=nf(data[code]["Talents"][const][1]),     inline=False)
        embed.set_footer(text="Constellation | "+str(id)+" | "+code+" | "+"7")
        Art = "https://gitlab.com/U-Eike/katheryne/-/raw/master/Images/Characters/"+ICode+"/Talents/0.png"
        embed.set_image(url=Art)

    else:
        ICode = code.replace(" ","_")
        embed = discord.Embed(
            title=code,
            description="Constellation "+str(id),
            url="https://www.gensh.in/characters/"+ICode.lower(),
            color=EMC[data[code]["Element"]]
        )
        Icon = "https://gitlab.com/U-Eike/katheryne/-/raw/master/Images/Characters/"+ICode+"/Talents/"+id+".png"
        embed.set_thumbnail(url=Icon)

        embed.add_field(name=nf(data[code]["Talents"][str(id)][0]),      value=nf(data[code]["Talents"][str(id)][1]),     inline=False)
        embed.set_footer(text="Constellation | "+str(id)+" | "+code+" | "+"7")
    return(embed)

def passive(code,id="0"):
    if id == "0":
        ICode = code.replace(" ","_")
        sel = int(id)
        print(code)
        embed = discord.Embed(
            title=code,
            description="Passives for "+code,
            url="https://www.gensh.in/characters/"+ICode.lower(),
            color=EMC[data[code]["Element"]]

        )
        Icon = "https://gitlab.com/U-Eike/katheryne/-/raw/master/Images/Characters/"+ICode+"/Passives/"+str(sel)+".png"
        embed.set_thumbnail(url=Icon)

        for passive in data[code]["Passives"]:
            embed.add_field(name=nf(passive[0]),      value=nf(passive[1]),     inline=False)
        embed.set_footer(text="Passive | "+str(int(id)+1)+" | "+code+" | "+"3")
        return(embed)

    else:
        ICode = code.replace(" ","_")
        sel = int(id)-1
        print(code)
        embed = discord.Embed(
            title=code,
            description="**__"+Pas[sel+1]+"__**",
            url="https://www.gensh.in/characters/"+ICode.lower(),
            color=EMC[data[code]["Element"]]

        )
        Icon = "https://gitlab.com/U-Eike/katheryne/-/raw/master/Images/Characters/"+ICode+"/Passives/"+str(sel+1)+".png"
        embed.set_thumbnail(url=Icon)

        embed.add_field(name=nf(data[code]["Passives"][sel][0]),      value=nf(data[code]["Passives"][sel][1]),     inline=False)
        embed.set_footer(text="Passive | "+str(sel)+" | "+code+" | "+"3")
        return(embed)

def repla(st):
    st = st.replace(" DMG Bonus","%")##Shortener
    st = st.replace("Base HP","HP")##Shortener
    st = st.replace("Base ATK","ATK")##Shortener
    st = st.replace("Base DEF","DEF")##Shortener
    st = st.replace("Base DEF","DEF")##Shortener
    st = st.replace("Level","Lvl")##Shortener
    return st

class Character_Commands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot


# def results(matches):
#    if (matches.len == 1):

    @commands.command(name='card'
    ,brief="Character Info Card"
    ,description="This command Brings up the intro details of a character"
    ,help="k.card \"Character name\" eg k.card Zho\nk.card \"Quick ID\" eg k.card 25\nThe id can be found when you try to enter a Character name and it matches multiple Characters")
    async def charactercard(self, ctx,  *args):


        if len(args) == 0:
            await ctx.send_help(Character_Commands.charactercard)
        else:

            code = args[0]
            hits = Char_Search(code)
            if(len(hits[0]) == 1):
                code = hits[0][0]
                mess = await ctx.send(embed=intro(code))
            else:
                await ctx.send(embed=Char_Returns(hits[0],ctx))

    @commands.command(name='HeadlightFluid',hidden=True)
    async def thiscommandshouldnotbeseenbyregularpeople(self, ctx,  *args):


        if len(args) == 0:
            await ctx.send_help(Character_Commands.charactercard)
        else:

            code = args[0]
            hits = Char_Search(code)
            if(len(hits[0]) == 1):
                code = hits[0][0]
                mess = await ctx.send(embed=intro(code))
                await mess.add_reaction("📊")
            else:
                Char_Returns(hits[0],ctx)

    @commands.command(name='evo'
    ,hidden=False)
    async def ascension(self, ctx,  *args):


        if len(args) == 0:
            await ctx.send_help(Character_Commands.charactercard)
        else:

            code = args[0]
            #lev = args[1]
            hits = Char_Search(code)
            if(len(hits[0]) == 1):
                code = hits[0][0]
                embed = ascension(code)
                mess = await ctx.send(embed=embed)
                await mess.add_reaction("📊")
                for Em in EMJDict["Ascension"]["All"]:
                    await mess.add_reaction(Em)
            elif(len(hits[0]) == 1) and (len(args) == 2):
                code = hits[0][0]
                lev = args[1]
                embed = ascension(code,lev)
                mess = await ctx.send(embed=embed)
                await mess.add_reaction("📊")



            else:
                embed = Char_Returns(hits[0])
                await ctx.send(embed=embed)

    @commands.command(name='char'
    ,brief="Character Basics"
    ,description="This command Brings up skill and intro details of a character"
    ,help="k.char \"Character name\" eg k.char Zho\nk.char \"Quick ID\" eg k.char 25\nThe id can be found when you try to enter a Character name and it matches multiple Characters")
    async def character(self, ctx,  *args):


        if len(args) == 0:
            await ctx.send_help(Character_Commands.character)
        else:

            code = args[0]
            hits = Char_Search(code)
            if(len(hits[0]) == 1):
                code = hits[0][0]
                mess = await ctx.send(embed=intro(code))
                await mess.add_reaction("📊")

                mess = await ctx.send(embed=natk(code),view=Table15())

                mess = await ctx.send(embed=skill(code),view=Table15())

                mess = await ctx.send(embed=burst(code),view=Table15())

                await mess.add_reaction("📊")


            else:
                embed = Char_Returns(hits[0])
                await ctx.send(embed=embed)

    @commands.command(name='full'
    ,brief="Full Character Data"
    ,description="This command Brings up the all the details of a character"
    ,help="k.full \"Character name\" eg k.full Zho\nk.full \"Quick ID\" eg k.full 25\nThe id can be found when you try to enter a Character name and it matches multiple Characters")
    async def everything(self, ctx,  *args):

        if len(args) == 0:
            await ctx.send_help(Character_Commands.everything)
        else:


            code = args[0]
            hits = Char_Search(code)
            if(len(hits[0]) == 1):
                code = hits[0][0]
                mess = await ctx.send(embed=intro(code))
                await mess.add_reaction("📊")

                mess = await ctx.send(embed=natk(code))
                await mess.add_reaction("📊")
                for Em in EMJDict["Skill Desc"]["All"]:
                    await mess.add_reaction(Em)

                mess = await ctx.send(embed=skill(code))
                await mess.add_reaction("📊")
                for Em in EMJDict["Skill Desc"]["All"]:
                    await mess.add_reaction(Em)

                mess = await ctx.send(embed=burst(code))
                await mess.add_reaction("📊")
                for Em in EMJDict["Skill Desc"]["All"]:
                    await mess.add_reaction(Em)

                mess = await ctx.send(embed=passive(code))
                await mess.add_reaction("📊")
                for Em in EMJDict["Passive"]["All"]:
                    await mess.add_reaction(Em)

                mess = await ctx.send(embed=stars(code))
                await mess.add_reaction("📊")
                for Em in EMJDict["Constellation"]["All"]:
                    await mess.add_reaction(Em)

                mess = await ctx.send(embed=history(code))
                await mess.add_reaction("📊")
                for Em in EMJDict["Story"]["All"]:
                    await mess.add_reaction(Em)

                mess = await ctx.send(embed=ascension(code))

            else:
                embed = Char_Returns(hits[0])
                await ctx.send(embed=embed)


    @commands.command(name='natk'
    ,brief="Character Normal Attack Data"
    ,description="This command Brings up the Normal Attack Data of a character"
    ,help="k.natk \"Character name\" eg k.natk Zho\nk.natk \"Quick ID\" eg k.natk 25\nThe id can be found when you try to enter a Character name and it matches multiple Characters")
    async def normalattack(self, ctx,  *args):
        if len(args) == 0:
            await ctx.send_help(Character_Commands.normalatk)
        else:

            code = args[0]
            hits = Char_Search(code)
            if(len(hits[0]) == 1):
                code = hits[0][0]

                if len(args) == 1:
                    mess = await ctx.send(embed=natk(code),view=Table15())
                else:
                    try:
                        mess = await ctx.send(embed=normalstats(code,args[1]),view=Table15())
                    except:
                        mess = await ctx.send(embed=natk(code),view=Table15())
            else:
                embed = Char_Returns(hits[0])
                await ctx.send(embed=embed)

    @commands.command(name='skill'
    ,brief="Character Elemental Skill Data"
    ,description="This command Brings up the Elemental Skill details of a character"
    ,help="k.skill \"Character name\" eg k.skill Zho\nk.skill \"Quick ID\" eg k.skill 25\nThe id can be found when you try to enter a Character name and it matches multiple Characters")
    async def elementalskill(self, ctx,  *args):

        if len(args) == 0:
            await ctx.send_help(Character_Commands.elementalskill)
        else:

            code = args[0]
            hits = Char_Search(code)
            if(len(hits[0]) == 1):
                code = hits[0][0]
                if len(args) == 1:
                    mess = await ctx.send(embed=skill(code),view=Table15())
                else:
                    try:
                        mess = await ctx.send(embed=skillstats(code,args[1]),view=Table15())
                    except:
                        mess = await ctx.send(embed=skill(code),view=Table15())


                await mess.add_reaction("📊")
                for Em in EMJDict["Skill Desc"]["All"]:
                    await mess.add_reaction(Em)

            else:
                embed = Char_Returns(hits[0])
                await ctx.send(embed=embed)

    @commands.command(name='burst'
    ,brief="Character Elemental Burst Data"
    ,description="This command Brings up the Elemental burst details of a character"
    ,help="k.burst \"Character name\" eg k.burst Zho\nk.burst \"Quick ID\" eg k.burst 25\nThe id can be found when you try to enter a Character name and it matches multiple Characters")
    async def elementalburst(self, ctx,  *args):


        if len(args) == 0:
            await ctx.send_help(Character_Commands.elementalburst)
        else:

            code = args[0]
            hits = Char_Search(code)
            if(len(hits[0]) == 1):
                code = hits[0][0]
                if len(args) == 1:
                    mess = await ctx.send(embed=burst(code),view=Table15())
                else:
                    try:
                        mess = await ctx.send(embed=burststats(code,args[1]),view=Table15())
                    except:
                        mess = await ctx.send(embed=burst(code),view=Table15())

                await mess.add_reaction("📊")
                for Em in EMJDict["Skill Desc"]["All"]:
                    await mess.add_reaction(Em)
            else:
                embed = Char_Returns(hits[0])
                await ctx.send(embed=embed)


    @commands.command(name='charsea'
    ,brief="Search Characters by tags"
    ,help="k.charsea \"Tags Seperated By space\" eg k.charsea geo pole \n\nThis will give you all characters that use a polearm and have the geo element.\nIf there is only one character matching the tags it will give you that instead\nA list of tags can be obtained using the command k.chartags")
    async def weaponChar_Search(self , ctx, *args):

        if len(args) == 0:
            await ctx.send_help(Character_Commands.weaponsearch)
        else:

            code = list(args)
            hits = searchtags(code)
            if(len(hits[0]) == 1):
                code = hits[0][0]
                embed = []
                embed.append(intro(code))
                for item in embed:
                    await ctx.send(embed=item)
            else:
                embed = Char_Returns(hits[0])
                await ctx.send(embed=embed)

    @commands.command(name='charstats'
    ,brief="Character Stat Table"
    ,description="This command pulls up a Characters Stat Table"
    ,help="k.charstats \"Character name\" eg k.charstats Zho\nk.charstats \"Quick ID\" eg k.charstats 25\nThe id can be found when you try to enter a weapon name and it matches multiple weapons")
    async def characterstats(self , ctx, *args):

        if len(args) == 0:
            await ctx.send_help(Character_Commands.character)


        code = args[0]
        hits = Char_Search(code)
        if(len(hits[0]) == 1):
            code = hits[0][0]
            mess = await ctx.send(embed=stats(code))
            await mess.add_reaction("📊")
        else:
            embed = Char_Returns(hits[0])
            await ctx.send(embed=embed)

    @commands.command(name='const'
    ,brief="Constallations for a character"
    ,description="This command Brings up the constallation talents of a character"
    ,help="k.const \"Character name\" eg k.const Zho\nk.const \"Quick ID\" eg k.const 25\nThe id can be found when you try to enter a Character name and it matches multiple Characters")

    async def constellation(self, ctx,  *args):

        if len(args) == 0:
            await ctx.send_help(Character_Commands.constellation)
        else:
            code = args[0]
            hits = Char_Search(code)
            if(len(hits[0]) == 1) and (len(args) == 1):
                code = hits[0][0]
                mess = await ctx.send(embed=stars(code),view=Choices6())

            elif(len(hits[0]) == 1) and (len(args) == 2):
                code = hits[0][0]
                mess = await ctx.send(embed=stars(code,args[1]),view=Choices6())
            else:
                embed = Char_Returns(hits[0])
                await ctx.send(embed=embed)

    @commands.command(name='pass'
    ,brief="Passive skills for a character"
    ,description="This command Brings up the Passive skills of a character"
    ,help="k.pass \"Character name\" eg k.pass Zho\nk.pass \"Quick ID\" eg k.pass 25\nThe id can be found when you try to enter a Character name and it matches multiple Characters")
    async def CharPass(self, ctx,  *args):

        if len(args) == 0:
            await ctx.send_help(Character_Commands.CharPass)
        else:

            code = args[0]
            hits = Char_Search(code)
            if(len(hits[0]) == 1) and (len(args) == 1):
                code = hits[0][0]
                mess = await ctx.send(embed=passive(code),view=ChoicesX(3))

            elif(len(hits[0]) == 1) and (len(args) == 2):
                code = hits[0][0]
                mess = await ctx.send(embed=passive(code,str(int(args[1])-1)),view=ChoicesX(3))
            else:
                embed = Char_Returns(hits[0])
                await ctx.send(embed=embed)

    @commands.command(name='story'    
    ,brief="Story for a character"
    ,description="This command Brings up the story behind a character"
    ,help="k.story \"Character name\" eg k.story Zho\nk.story \"Quick ID\" eg k.story 25\nThe id can be found when you try to enter a Character name and it matches multiple Characters")

    async def backstory(self, ctx,  *args):

        if len(args) == 0:
            await ctx.send_help(Character_Commands.backstory)
        else:

            code = args[0]
            hits = Char_Search(code)
            if(len(hits[0]) == 1) and (len(args) == 1):
                code = hits[0][0]
                mess = await ctx.send(embed=history(code),view=StoryChoice())

            elif(len(hits[0]) == 1) and (len(args) == 2):
                code = hits[0][0]
                mess = await ctx.send(embed=history(code,args[1]),view=StoryChoice())

            else:
                embed = Char_Returns(hits[0])
                await ctx.send(embed=embed)

    @commands.command(name='chartags',brief="List of character tags")
    async def weapontags(self , ctx, *args):

        item = rettags()
        await ctx.send(embed=item)


############################Button Controls############################
class Table15Buttons(discord.ui.Button['Table15']):
    def __init__(self, x: int, y: int, tex: str):
        super().__init__(style=discord.ButtonStyle.secondary, label=tex, row=y,custom_id=str(x)+str(y))#\u200b
        self.x = x
        self.y = y

    async def callback(self, interaction: discord.Interaction):
        assert self.view is not None
        view: Table15 = self.view
        tex = self.label
        if tex == "🗑️":
            await interaction.message.delete()
        else:
            Treemod = interaction.message.embeds[0].footer.text.split(" | ")
            if tex == "📖":
                if Treemod[2] == "Normal":
                    await interaction.response.edit_message(embed=normalstats(Treemod[1]), view=view)
                elif Treemod[2] == "Skill":
                    await interaction.response.edit_message(embed=skillstats(Treemod[1]), view=view)
                elif Treemod[2] == "Burst":
                    await interaction.response.edit_message(embed=burststats(Treemod[1]), view=view)
            else:
                if Treemod[2] == "Normal":
                    await interaction.response.edit_message(embed=normalstats(Treemod[1],tex), view=view)
                elif Treemod[2] == "Skill":
                    await interaction.response.edit_message(embed=skillstats(Treemod[1],tex), view=view)
                elif Treemod[2] == "Burst":
                    await interaction.response.edit_message(embed=burststats(Treemod[1],tex), view=view)

class Table15(discord.ui.View):
    children: List[Table15Buttons]
    X = -1
    O = 1
    Tie = 2

    def __init__(self):
        super().__init__(timeout=None)
        for x in range(5):
            for y in range(3):
                Lab = str(5*(y)+x+1)
                self.add_item(Table15Buttons(x, y,Lab))
        self.add_item(Table15Buttons(1, 4,"📊"))
        self.add_item(Table15Buttons(2, 4,"📖"))
        self.add_item(Table15Buttons(3, 4,"🗑️"))

class Choices6Buttons(discord.ui.Button['Choices6']):
    def __init__(self, x: int, y: int, tex: str):
        super().__init__(style=discord.ButtonStyle.secondary, label=tex, row=y,custom_id=str(x)+str(y))#\u200b
        self.x = x
        self.y = y

    async def callback(self, interaction: discord.Interaction):
        assert self.view is not None
        view: Choices6 = self.view
        tex = self.label
        #print("Ehhhhhhhhh")
        if tex == "🗑️":
            await interaction.message.delete()
        else:
            Treemod = interaction.message.embeds[0].footer.text.split(" | ")
            if tex == "📖":
                if Treemod[0] == "Constellation":
                    await interaction.response.edit_message(embed=stars(Treemod[2]), view=view)
            else:
                if Treemod[0] == "Constellation":
                    await interaction.response.edit_message(embed=stars(Treemod[2],tex), view=view)
class Choices6(discord.ui.View):
    children: List[Choices6Buttons]
    X = -1
    O = 1
    Tie = 2

    def __init__(self):
        super().__init__(timeout=None)
        for x in range(3):
            for y in range(2):
                Lab = str(3*(y)+x+1)
                self.add_item(Choices6Buttons(x, y,Lab))
        self.add_item(Choices6Buttons(4, 0,"📖"))
        self.add_item(Choices6Buttons(4, 1,"🗑️"))

class ChoicesXButtons(discord.ui.Button['ChoicesX']):
    def __init__(self, x: int, y: int, tex: str):
        super().__init__(style=discord.ButtonStyle.secondary, label=tex, row=y,custom_id=str(x)+str(y))#\u200b
        self.x = x
        self.y = y

    async def callback(self, interaction: discord.Interaction):
        assert self.view is not None
        view: ChoicesX = self.view
        tex = self.label
        #print("Ehhhhhhhhh")
        if tex == "🗑️":
            await interaction.message.delete()
        else:
            Treemod = interaction.message.embeds[0].footer.text.split(" | ")
            if tex == "📖":
                if Treemod[0] == "Constellation":
                    await interaction.response.edit_message(embed=stars(Treemod[2]), view=view)
                if Treemod[0] == "Passive":
                    await interaction.response.edit_message(embed=passive(Treemod[2]), view=view)
            elif tex in ["1","2","3","4","5","6"]:
                if Treemod[0] == "Constellation":
                    await interaction.response.edit_message(embed=stars(Treemod[2],tex), view=view)
                if Treemod[0] == "Passive":
                    await interaction.response.edit_message(embed=passive(Treemod[2],tex), view=view)
class ChoicesX(discord.ui.View):
    children: List[ChoicesXButtons]
    X = -1
    O = 1
    Tie = 2

    def __init__(self,Choices=3):
        super().__init__(timeout=None)
        for x in range(Choices):
            Lab = str(x+1)
            self.add_item(ChoicesXButtons(x, 0,Lab))
        self.add_item(ChoicesXButtons(0, 4,"📖"))
        self.add_item(ChoicesXButtons(1, 4,"🗑️"))


class StoryChoiceButtons(discord.ui.Button['StoryChoice']):
    def __init__(self, x: int, y: int, tex: str="",**kwargs):
        super().__init__(style=discord.ButtonStyle.secondary, label=tex,row=y,custom_id=str(x)+str(y))#\u200b
        self.emoji = kwargs.get("emoji")
        self.x = x
        self.y = y

    async def callback(self, interaction: discord.Interaction):
        assert self.view is not None
        view: StoryChoice = self.view
        tex = self.label
        #print("Ehhhhhhhhh")
        if tex == "🗑️":
            await interaction.message.delete()
        else:
            Treemod = interaction.message.embeds[0].footer.text.split(" | ")
            if tex == "📖":
                if Treemod[0] == "Constellation":
                    await interaction.response.edit_message(embed=stars(Treemod[2]), view=view)
                if Treemod[0] == "Story ":
                    await interaction.response.edit_message(embed=history(Treemod[2],"0"), view=view)
            elif tex in ["1","2","3","4","5","6"]:
                if Treemod[0] == "Story":
                    await interaction.response.edit_message(embed=history(Treemod[2],tex), view=view)
            elif self.emoji.name == "📖":
                if Treemod[0] == "Story":
                    await interaction.response.edit_message(embed=history(Treemod[2],"0"), view=view)
            elif self.emoji.name == "TravNul":
                if Treemod[0] == "Story":
                    await interaction.response.edit_message(embed=history(Treemod[2],"7"), view=view)
            elif self.emoji.name == "Primo":
                if Treemod[0] == "Story":
                    await interaction.response.edit_message(embed=history(Treemod[2],"6"), view=view)

class StoryChoice(discord.ui.View):
    children: List[StoryChoiceButtons]
    X = -1
    O = 1
    Tie = 2

    def __init__(self):
        super().__init__(timeout=None)
        self.add_item(StoryChoiceButtons(0, 0,emoji ="📖"))
        self.add_item(StoryChoiceButtons(0, 1,emoji ="🗑️"))
        for x in range(3):
            for y in range(2):
                Lab = str(3*(y)+x+1)
                self.add_item(StoryChoiceButtons(1+x, y,Lab))
        self.add_item(StoryChoiceButtons(4, 0,emoji="<:Primo:891152938397999144>"))
        self.add_item(StoryChoiceButtons(4, 1,emoji="<:TravNul:780669929375793152>"))




def setup(bot):
    bot.add_cog(Character_Commands(bot))
