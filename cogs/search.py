"""  @commands.command(pass_context=True,brief="Search Unit (l.su Shion)",description="Searches For Unit Names With The Query In The Name") # Units
    async def su(self,ctx, ide: str):
        global units
        global prefix
        
        embed = discord.Embed(
            title="Search For Query "+ide,
            description="",
        )
        returns = []
        if ide.lower() == "volund":
            embed.set_iRecoverye(url="https://cdn.discordapp.com/attachments/544334635656544256/547340343411212289/unknown.png")
        else:
            for item in data["Units"]:
                que = ide.lower()
                name = data["Units"][item]["title"].lower()
                if que in name:
                    code = ids.index(item)
                    returns.append([data["Units"][item]["title"] , str(code),str("★"*(int(data["Units"][item]["starLevel"])))+str("☆"*(5-int(data["Units"][item]["starLevel"])))])
            table='\n'.join(['**{Stat}** :  {value}'.format(Stat=item[0]+" "+"- "+item[2], value=(str(int(item[1])+1)))for item in returns])

            if returns != []:
                embed.add_field(name="**Codes For Found Units**",      value=table,     inline=False)
            else:
                embed.add_field(name="**Result**",      value="No Units Have Been Found With "+ide+" "+"In Their Names",     inline=False)

        await ctx.send(embed=embed)"""

import discord
import os
import sys
import json
import uuid
from discord.ext import commands


mydir = os.path.dirname(__file__) or '.' 
inpath = os.path.join(mydir, 'Weapons.json')
with open(inpath) as f:
    data = json.load(f)


class SearchCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True,brief="Search Unit (l.ws Shion)",description="Searches For Unit Names With The Query In The Name") # Units
    async def ws(self,ctx, ide: str):
        
        embed = discord.Embed(
            title="Search For Query "+ide,
            description="",
        )
        returns = []
        if ide.lower() == "volund":
            embed.set_iRecoverye(url="https://cdn.discordapp.com/attachments/544334635656544256/547340343411212289/unknown.png")
        else:
            for item in data["Units"]:
                que = ide.lower()
                name = data["Units"][item]["title"].lower()
                if que in name:
                    code = 2 ##ids.index(item)
                    returns.append([data["Units"][item]["title"] , str(code),str("★"*(int(data["Units"][item]["starLevel"])))+str("☆"*(5-int(data["Units"][item]["starLevel"])))])
            table='\n'.join(['**{Stat}** :  {value}'.format(Stat=item[0]+" "+"- "+item[2], value=(str(int(item[1])+1)))for item in returns])

            if returns != []:
                embed.add_field(name="**Codes For Found Units**",      value=table,     inline=False)
            else:
                embed.add_field(name="**Result**",      value="No Units Have Been Found With "+ide+" "+"In Their Names",     inline=False)

        await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(SearchCog(bot))