from typing import List
from discord.ext import commands
import discord

# Defines a custom button that contains the logic of the game.
# The ['TicTacToe'] bit is for type hinting purposes to tell your IDE or linter
# what the type of `self.view` is. It is not required.

def keydefuscator():
    f = open("tk.txt", "r")
    tk = f.read()
    tk1 = tk[0:int(len(tk)/2)]
    tk2 = tk[int(len(tk)/2):len(tk)]
    tkf = tk2+tk1
    return(tkf)


class ControlButtons(discord.ui.Button['Controls']):
    def __init__(self, x: int, y: int, tex: str):
        super().__init__(style=discord.ButtonStyle.secondary, label=tex, row=y)#\u200b
        self.x = x
        self.y = y

    # This function is called whenever this particular button is pressed
    # This is part of the "meat" of the game logic
    async def callback(self, interaction: discord.Interaction):
        assert self.view is not None
        view: Controls = self.view
        tex = self.label
        await interaction.response.edit_message(content=str(tex), view=view)

# This is our actual board View
class Controls(discord.ui.View):
    children: List[ControlButtons]
    X = -1
    O = 1
    Tie = 2

    def __init__(self):
        super().__init__()
        for x in range(5):
            for y in range(3):
                Lab = str(5*(y)+x)
                self.add_item(ControlButtons(x, y,Lab))


class TicTacToeBot(commands.Bot):
    def __init__(self):
        super().__init__(command_prefix=commands.when_mentioned_or('$'))

    async def on_ready(self):
        print(f'Logged in as {self.user} (ID: {self.user.id})')
        print('------')


bot = TicTacToeBot()


@bot.command()
async def tic(ctx: commands.Context):
    """Starts a tic-tac-toe game with yourself."""
    await ctx.send('Tic Tac Toe: X goes first', view=Controls())


tok = keydefuscator()
BOT_TOKEN = tok
bot.run(BOT_TOKEN)
